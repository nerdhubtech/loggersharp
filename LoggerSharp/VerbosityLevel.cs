﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSharp
{
    public struct VerbosityLevel
    {
        //TODO: Translate LogStatus to Verbosity. However, Check specific verbosity states.
        private LogStatus[] accepts;

        VerbosityLevel(params LogStatus[] accepts)
        {
            this.accepts = accepts;
        }

        internal LogStatus[] GetAccepts() => accepts;

        /// <summary>
        /// Means NO output is to be returned, logging is forced to OFF.
        /// </summary>
        public static VerbosityLevel OFF = new VerbosityLevel(LogStatus.OFF);
        /// <summary>
        /// Means only INFO status is to be returned, logging will only accept INFO states.
        /// </summary>
        public static VerbosityLevel INFO = new VerbosityLevel(LogStatus.INFO);
        /// <summary>
        /// Means INFO and WARNING is to be returned, logging will only accept INFO or WARNING states.
        /// </summary>
        public static VerbosityLevel WARNING = new VerbosityLevel(LogStatus.INFO, LogStatus.WARNING);
        /// <summary>
        /// Means INFO, WARNING, and ERROR is to be returned, logging will only accept INFO, WARNING, or ERROR states.
        /// </summary>
        public static VerbosityLevel ERROR = new VerbosityLevel(LogStatus.INFO, LogStatus.WARNING, LogStatus.ERROR);
        public static VerbosityLevel SEVERE = new VerbosityLevel(LogStatus.INFO, LogStatus.WARNING, LogStatus.ERROR, LogStatus.SEVERE);
        public static VerbosityLevel FATAL = new VerbosityLevel(LogStatus.INFO, LogStatus.WARNING, LogStatus.ERROR, LogStatus.SEVERE, LogStatus.FATAL);
        public static VerbosityLevel DEBUG = new VerbosityLevel(LogStatus.INFO, LogStatus.DEBUG_INFO, LogStatus.DEBUG_WARNING, LogStatus.DEBUG_ERROR, LogStatus.DEBUG_SEVERE, LogStatus.DEBUG_FATAL);
        public static VerbosityLevel ALL = new VerbosityLevel(LogStatus.DEBUG_FATAL, LogStatus.DEBUG_SEVERE, LogStatus.DEBUG_ERROR, LogStatus.DEBUG_WARNING, LogStatus.DEBUG_INFO, LogStatus.INFO, LogStatus.WARNING, LogStatus.ERROR, LogStatus.SEVERE, LogStatus.FATAL);
    }
}
