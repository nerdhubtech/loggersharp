using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace LoggerSharp
{
    internal class LoggerFactory : IEnumerable<Logger>
    {
        public static LoggerFactory INSTANCE = new LoggerFactory();

        private List<Logger> loggers;

        public IEnumerator<Logger> GetEnumerator()
            => loggers.GetEnumerator();

        IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();

        public IReadOnlyCollection<Logger> GetLoggers() => loggers;

        LoggerFactory()
        {
            loggers = new List<Logger>();
        }

        public bool Contains(Logger logger)
        {
            foreach (Logger l in this)
            {
                if (l.Equals(logger))
                    return true;
                else continue;
            }
            return false;
        }

        public void Add(Logger logger)
        {
            if (!Contains(logger))
            {
                loggers.Add(logger);
                if (!(logger is InternalLogger))
                {
                    var lggr = InternalLogger.INSTANCE;
                    if (lggr != null)
                        lggr.LogDebugInfo(string.Format("The logger: \"{0}\", was added to the factory.", logger.GetTag()));
                }
            }
        }

        public void Remove(Logger logger)
        {
            if (Contains(logger) && !(logger is InternalLogger))
                loggers.Remove(logger);
        }

        public int? IndexOf(Logger logger)
        {
            for (int i = 0; i < Count; i++)
            {
                var l = this[i];
                if (l.Equals(logger))
                    return i;
                else continue;
            }
            return null;
        }

        public Logger this[int i]
        {
            get => loggers[i];
            set => loggers[i] = value;
        }

        public Logger this[string tag]
        {
            get => GetLogger(tag);
            set => this[(int)IndexOf(GetLogger(tag))] = value;
        }

        public Logger this[object owner]
        {
            get => GetLogger(owner);
            set => this[(int)IndexOf(GetLogger(owner))] = value;
        }

        public Logger GetLogger(string tagName)
        {
            foreach (Logger logger in this)
            {
                var tagx = logger.GetTag();
                if (tagx.StartsWith("[") && tagx.EndsWith("]"))
                {
                    tagx = tagx.Remove(0, 1);
                    tagx = tagx.Remove(tagx.Length - 1, 1);
                }
                if (tagx.Equals(tagName, StringComparison.OrdinalIgnoreCase))
                    return logger;
                else continue;
            }
            return null;
        }

        public Logger GetLogger(object owner)
        {
            foreach (Logger logger in this)
            {
                if (logger.GetOwner().Equals(owner))
                    return logger;
                else continue;
            }
            return null;
        }

        public int Count => loggers.Count;

        public bool IsEmpty() => Count <= 0;

        public Logger CreateGeneric(object owner, bool debug = false, bool timestamp = true)
        {
            return Create(owner, owner.GetType().Name, debug, timestamp, (DirectoryInfo)null);
        }

        public Logger Create(object owner, string tag = null, bool debug = false, bool timestamp = true, FileInfo logFile = null)
        {
            Logger logger = new Logger(owner, tag, debug, timestamp, logFile);
            //Register the logger with the factory.
            Add(logger);
            //Get the logger from the factory and return the created one only as a fallback.
            return this[owner];
        }

        public Logger Create(object owner, string tag = null, bool debug = false, bool timestamp = true, DirectoryInfo logFile = null)
        {
            Logger logger = new Logger(owner, tag, debug, timestamp, logFile);
            //Register the logger.
            Add(logger);
            //Get the logger from the factory and return the created one only as a fallback.
            return this[owner];
        }

        public Logger Set(object o, Logger logger)
        {
            var debug = logger.Debug;
            var timestamp = logger.IsUsingLongTimestamp;
            var tag = logger.GetTag();
            var logFile = logger.GetLogFile();
            if (Contains(GetLogger(o)))
                Remove(GetLogger(o));
            if (!(logFile == null))
            {
                var x = logFile.GetLogFile();
                logger.SetOwner(o); //Overrides the owner.
                                    //Delete the old log file.
                logFile.Delete();
                if (logFile.GetLogFileType() == LogFile.LogFileType.DIRECTORY)
                    logger.SetLogFile(new LogFile(new DirectoryInfo(x.FullName)));
                else
                    logger.SetLogFile(new LogFile(new FileInfo(x.FullName)));
            }
            return this[o];
        }
    }
}