using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace LoggerSharp
{
    ///<summary>
    /// Handles logging to a logfile instead of the terminal/console.
    ///</summary>
    public sealed class LogFile
    {
        private FileInfo fileData;
        private LogFileType logFileType;

        public LogFile(DirectoryInfo dir)
        {
            //Check if the parent directory exists.
            if (!(dir.Exists))
                dir.Create(); //If not, create it.
            
            //Create the log file.
            fileData = new FileInfo(Path.GetFullPath($"{dir}/{GetTimestamp(false)}.log"));
            if (!(fileData.Exists))
                fileData.Create().Close(); //Close the file.
                
            logFileType = LogFileType.DIRECTORY;
        }

        public LogFile(FileInfo file)
        {
            //Check if the file ends with '.log'.
            if (!(file.FullName.EndsWith(Path.GetExtension(".log"))))
                file = new FileInfo(file.FullName + ".log"); //Set the new extension.
            fileData = file;

            //Check if the parent directory exists.
            DirectoryInfo dir = file.Directory;
            if (!(dir.Exists))
                dir.Create(); //If not, create it.
            
            //Create the log file.
            if (!(fileData.Exists))
                fileData.Create().Close(); //Close the file.
                
            logFileType = LogFileType.FILE;

            //Write information for this session.
            var ts = GetTimestamp(true);
            var read = Read(out string output);
            if (read)
                Write($"-----------{new string('-', ts.Length)}-----------");
            Write($"----------|{ts}|----------");
        }

        public FileStream GetFileStream() => new FileStream(fileData.FullName, FileMode.OpenOrCreate, FileAccess.ReadWrite, FileShare.ReadWrite);

        public StreamReader GetReader() => new StreamReader(GetFileStream());
        public StreamWriter GetWriter() => new StreamWriter(GetFileStream());

        internal LogFileType GetLogFileType() => logFileType;
        internal FileInfo GetLogFile() => fileData;

        //Read the log file.
        internal bool Read(out string output)
        {
            try
            {
                using (StreamReader rdr = GetReader())
                {
                    var x = rdr.ReadToEnd();
                    if (!(string.IsNullOrEmpty(x) && string.IsNullOrWhiteSpace(x)))
                    {
                        output = x;
                        return true;
                    }
                    else
                    {
                        output = "";
                        return false;
                    }
                }
            }
            catch (Exception ex)
            {
                InternalLogger.INSTANCE.LogException("Could not read the LogFile", ex);
                output = null;
                return false;
            }
        }
        
        //Write text to the log file.
        public void Write(string text)
        {
            try
            {
                using (StreamWriter wtr = GetWriter())
                {
                    var read = Read(out string data);
                    if (read)
                        wtr.Write($"{data}\n{text}");
                    else
                        wtr.Write(text);
                }
            }
            catch (Exception ex)
            {
                InternalLogger.INSTANCE.LogException("Could not write to LogFile", ex);
            }
        }

        //Gets a timestamp.
        private string GetTimestamp(bool isInsideFile)
        {
            if (isInsideFile)
                return $"{DateTime.Now.ToString("yyyy-MM-dd @ HH:mm:ss")}";
            else
                return $"{DateTime.Now.ToString("yyyy_MM_dd-HH_mm_ss")}";
        }
        
        public enum LogFileType
        {
            DIRECTORY,
            FILE
        }

        internal void Delete()
        {
            throw new NotImplementedException();
        }
    }
}