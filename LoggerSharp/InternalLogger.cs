using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using LoggerSharp.Linq;

namespace LoggerSharp
{
    public sealed class InternalLogger : Logger
    {
        public static InternalLogger INSTANCE = new InternalLogger();

        private static readonly string LoggerName = nameof(Logger);

        private InternalLogger() : base(null, LoggerName, false, true, new FileInfo($"{Directory.GetCurrentDirectory()}/logger.log"))
        {
#if DEBUG
            Debug = true;
#endif
            Label = nameof(Logger);
        }

        public void LogException(string caption, Exception ex)
        {
            StringBuilder builder = new StringBuilder();
            foreach (string ln in ex.ToString().Split(Environment.NewLine))
            {
                if (builder.Length > 0)
                    builder.Append("\n    " + ln);
                else
                    builder.Append("    " + ln);
            }

        }

        public Logger GetLogger() => this;
    }
}