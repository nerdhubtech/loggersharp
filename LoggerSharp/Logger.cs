using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

using LoggerSharp.Linq;

namespace LoggerSharp
{
    public class Logger
    {
        //Get a logfile.
        public static LogFile GetLogFile(string file) => new LogFile(new FileInfo($"{Directory.GetCurrentDirectory()}/Logs/{file}.log"));

        private string loggerTag;
        private object owner;
        private bool useLongTimestamp, debug;
        private LogFile logFile = null;
        private InternalLogger internalLogger;
        private VerbosityLevel verbosity = VerbosityLevel.ALL;

        public VerbosityLevel Verbosity
        {
            get => verbosity;
            set => verbosity = value;
        }

        ///<summary>
        /// Create a new instance of <see cref="Logger">.
        ///</summary>
        ///<param name="owner">The owner of the logger/logfile.</param>
        ///<param name="debug">Sets the debug state. Requred to return debug statuses.</param>
        ///<param name="useLongTimestamp">Denotes whether the date should be included in the log.</param>
        public Logger(object owner, bool debug = false, bool useLongTimestamp = true)
        {
            this.loggerTag = null;
            this.owner = owner ?? this;
            this.debug = debug;
            this.useLongTimestamp = useLongTimestamp;
            internalLogger = InternalLogger.INSTANCE;
            //Register Logger with the Factory.
            var factory = LoggerFactory.INSTANCE;
            if (!factory.Contains(this))
                factory.Add(this);
        }

        ///<summary>
        /// Create a new instance of <see cref="Logger">.
        ///</summary>
        ///<param name="owner">The owner of the logger/logfile.</param>
        ///<param name="debug">Sets the debug state. Requred to return debug statuses.</param>
        ///<param name="useLongTimestamp">Denotes whether the date should be included in the log.</param>
        ///<param name="logFile">Denotes the file all logs will be logged to.</param>
        public Logger(object owner, bool debug = false, bool useLongTimestamp = true, FileInfo logFile = null)
        {
            this.loggerTag = null;
            this.owner = owner ?? this;
            this.debug = debug;
            this.useLongTimestamp = useLongTimestamp;
            this.logFile = (!(logFile == null)) ? new LogFile(logFile) : null;
            internalLogger = InternalLogger.INSTANCE;
            //Register Logger with the Factory.
            var factory = LoggerFactory.INSTANCE;
            if (!factory.Contains(this))
                factory.Add(this);
        }

        ///<summary>
        /// Create a new instance of <see cref="Logger">.
        ///</summary>
        ///<param name="owner">The owner of the logger/logfile.</param>
        ///<param name="debug">Sets the debug state. Requred to return debug statuses.</param>
        ///<param name="useLongTimestamp">Denotes whether the date should be included in the log.</param>
        ///<param name="logFile">Denotes the directory in which log files will be created.</param>
        public Logger(object owner, bool debug = false, bool useLongTimestamp = true, DirectoryInfo logFile = null)
        {
            this.loggerTag = null;
            this.owner = owner ?? this;
            this.debug = debug;
            this.useLongTimestamp = useLongTimestamp;
            this.logFile = (!(logFile == null)) ? new LogFile(logFile) : null;
            internalLogger = InternalLogger.INSTANCE;
            //Register Logger with the Factory.
            var factory = LoggerFactory.INSTANCE;
            if (!factory.Contains(this))
                factory.Add(this);
        }

        ///<summary>
        /// Create a new instance of <see cref="Logger">.
        ///</summary>
        ///<param name="owner">The owner of the logger/logfile.</param>
        ///<param name="loggerTag">Denotes the custom label of the logger.</param>
        ///<param name="debug">Sets the debug state. Requred to return debug statuses.</param>
        ///<param name="useLongTimestamp">Denotes whether the date should be included in the log.</param>
        ///<param name="logFile">Denotes the file all logs will be logged to.</param>
        public Logger(object owner, string loggerTag, bool debug = false, bool useLongTimestamp = true)
        {
            this.loggerTag = loggerTag;
            this.owner = owner ?? this;
            this.debug = debug;
            this.useLongTimestamp = useLongTimestamp;
            internalLogger = InternalLogger.INSTANCE;
            //Register Logger with the Factory.
            var factory = LoggerFactory.INSTANCE;
            if (!factory.Contains(this))
                factory.Add(this);
        }

        ///<summary>
        /// Create a new instance of <see cref="Logger">.
        ///</summary>
        ///<param name="owner">The owner of the logger/logfile.</param>
        ///<param name="loggerTag">Denotes the custom label of the logger.</param>
        ///<param name="debug">Sets the debug state. Requred to return debug statuses.</param>
        ///<param name="useLongTimestamp">Denotes whether the date should be included in the log.</param>
        ///<param name="logFile">Denotes the file all logs will be logged to.</param>
        ///<param name="logFile">Denotes the directory in which log files will be created.</param>
        public Logger(object owner, string loggerTag, bool debug = false, bool useLongTimestamp = true, FileInfo logFile = null)
        {
            this.loggerTag = loggerTag;
            this.owner = owner ?? this;
            this.debug = debug;
            this.useLongTimestamp = useLongTimestamp;
            this.logFile = (!(logFile == null)) ? new LogFile(logFile) : null;
            internalLogger = InternalLogger.INSTANCE;
            //Register Logger with the Factory.
            var factory = LoggerFactory.INSTANCE;
            if (!factory.Contains(this))
                factory.Add(this);
        }

        ///<summary>
        /// Create a new instance of <see cref="Logger">.
        ///</summary>
        ///<param name="owner">The owner of the logger/logfile.</param>
        ///<param name="loggerTag">Denotes the custom label of the logger.</param>
        ///<param name="debug">Sets the debug state. Requred to return debug statuses.</param>
        ///<param name="useLongTimestamp">Denotes whether the date should be included in the log.</param>
        ///<param name="logFile">Denotes the file all logs will be logged to.</param>
        ///<param name="logFile">Denotes the file all logs will be logged to.</param>
        public Logger(object owner, string loggerTag, bool debug = false, bool useLongTimestamp = true, DirectoryInfo logFile = null)
        {
            this.loggerTag = loggerTag;
            this.owner = owner ?? this;
            this.debug = debug;
            this.useLongTimestamp = useLongTimestamp;
            this.logFile = (!(logFile == null)) ? new LogFile(logFile) : null;
            internalLogger = InternalLogger.INSTANCE;
            var factory = LoggerFactory.INSTANCE;
            if (!factory.Contains(this))
                factory.Add(this);
        }

        ///<summary>
        /// Gets the owner of the logger/logfile.
        ///</summary>
        public object GetOwner() => owner;
        ///<summary>
        /// Gets the tag or sender of the logger.
        ///</summary>
        public string GetTag()
        {
            var x = !(string.IsNullOrEmpty(loggerTag) && string.IsNullOrWhiteSpace(loggerTag));
            var _x = GetOwner() != null;
            return (x) ? loggerTag : ((_x) ? GetOwner().ToString().ToUpper() : GetType().Name.ToUpper());
        }
        ///<summary>
        /// Checks if the logger is using a long or short timestamp.
        ///</summary>
        public bool IsUsingLongTimestamp => useLongTimestamp;

        ///<summary>
        /// Gets or sets the debug state.
        ///</summary>
        public bool Debug 
        {
            get => debug;
            set => debug = value;
        }

        ///<summary>
        /// Gets the underlying logfile if one was created.
        ///</summary>
        public LogFile GetLogFile() => logFile;
        // Set the logfile. Used by loggerfactory to override logger logfile.
        internal void SetLogFile(LogFile logFile)
        {
            //Create a new log file manually rather then overriding the existing instance.

        }
        // Set the owner. Used by loggerfactory to override the logger owner.
        internal void SetOwner(object owner) => this.owner = owner;
        // Get the timestamp.
        internal string GetTimestamp()
            => (IsUsingLongTimestamp) ? DateTime.Now.ToString("yyyy-MM-dd @ HH:mm:ss") : DateTime.Now.ToString("HH:mm:ss");
        //Resolve status.
        internal string ResolveStatus(LogStatus status)
        {
            var x = status.GetStatusName();
            if (x.Contains("_"))
            x = x.Replace("_", "][");
            return $"[{x}]";
        }
        //Checks if the status provided is that of the debug state(s).
        private bool IsDebugStatus(LogStatus status)
        {
            var x = status.GetStatus();
            if (x == -1 || x == -2 || x == -3 || x == -4 || x == -5)
                return true;
            return false;
        }

        ///<summary>
        /// Log output.
        ///</summary>
        ///<param name="status">Denotes the log status.</param>
        ///<param name="message">Denotes the text to log</param>
        public void Log(LogStatus status, string message)
        {
            try
            {
                var sender = $"[{GetTag()}]";
                var timestamp = $"[{GetTimestamp()}]";
                var statusTag = ResolveStatus(status);
                var colx = Console.ForegroundColor;
                var color = (status.GetStatusColor() != null) ? (ConsoleColor)status.GetStatusColor() : colx;
                if (!(status.Equals(LogStatus.OFF)) || Verbosity.Equals(VerbosityLevel.OFF))
                {
                    if (IsDebugStatus(status) && Debug)
                    {
                        if (Verbosity.GetAccepts().Contains(status))
                        {
                            var res = $"{timestamp}{sender}{statusTag}: {message}";
                            if (!(IsDebugStatus(status)))
                            {
                                Console.ForegroundColor = color;
                                if (status.Equals(LogStatus.ERROR) || status.Equals(LogStatus.FATAL) || status.Equals(LogStatus.SEVERE))
                                    Console.Error.WriteLine(res);
                                else
                                    Console.Out.WriteLine(res);
                                Console.ForegroundColor = colx; //Reset.
                                if (!(logFile == null))
                                    logFile.Write(res);
                            }
                            //else: Treat the output as if it were OFF.
                        }
                    }
                    else
                    {
                        if (Verbosity.GetAccepts().Contains(status))
                        {
                            var res = $"{timestamp}{sender}{statusTag}: {message}";
                            if (!(IsDebugStatus(status)))
                            {
                                Console.ForegroundColor = color;
                                if (status.Equals(LogStatus.ERROR) || status.Equals(LogStatus.FATAL) || status.Equals(LogStatus.SEVERE))
                                    Console.Error.WriteLine(res);
                                else
                                    Console.Out.WriteLine(res);
                                Console.ForegroundColor = colx; //Reset.
                                if (!(logFile == null))
                                    logFile.Write(res);
                            }
                            //else: Treat the output as if it were OFF.
                        }
                    }
                }
            }
            catch (IOException ex)
            {
                GetInternalLogger().LogException("Could not create or write to log file...", ex);
            }
            catch (Exception ex)
            {
               GetInternalLogger().LogException("An Unknown Exception Was Thrown...", ex);
            }
        }
        
        public void CreateNewLogFile(FileInfo file)
        {
            SetLogFile(new LogFile(file));
        }
        
        public void CreateNewLogFile(DirectoryInfo directory)
        {
            SetLogFile(new LogFile(directory));
        }

        ///<summary>
        /// Log output.
        ///</summary>
        ///<param name="status">Denotes the log status.</param>
        ///<param name="message">Denotes the text to log</param>
        ///<param name="output">Represents additional data to write to the log.</param>
        public void Log(LogStatus status, string message, List<string> output)
        {
            string s = "";
            foreach (string x in output)
            {
                if (s.Length > 0)
                    s += $"\n    - {x}";
                else
                    s = $"    - {x}";
            }
            s = message + "\n" + s;
            Log(status, s); //Log normally.
        }

        ///<summary>
        /// Log output using the info status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogInfo(string message)
            => Log(LogStatus.INFO, message);

        ///<summary>
        /// Log output using the warning status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogWarning(string message)
            => Log(LogStatus.WARNING, message);

        ///<summary>
        /// Log output using the error status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogError(string message)
            => Log(LogStatus.ERROR, message);

        public void LogError(string message, Exception ex)
        {
            StringBuilder result = new StringBuilder();
            var x = ex.ToString();
            foreach (string s in x.Split(Environment.NewLine))
            {
                if (result.Length > 0)
                    result.Append($"\n    {s}");
                else
                    result.Append($"    {s}");
            }
            LogError($"{message} -> \n{result}");
        }

        ///<summary>
        /// Log output using the severe status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogSevere(string message)
            => Log(LogStatus.SEVERE, message);
        
        ///<summary>
        /// Log output using the fatal status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogFatal(string message)
            => Log(LogStatus.FATAL, message);

        ///<summary>
        /// Log output using the debug/info status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogDebugInfo(string message)
            => Log(LogStatus.DEBUG_INFO, message);

        ///<summary>
        /// Log output using the debug/error status.
        ///</summary>
        ///<param name="message">Denotes the text to log</param>
        public void LogDebugError(string message)
            => Log(LogStatus.DEBUG_ERROR, message);
        
        [Obsolete("This function was integrated into the specified error functions. All you need is to pass an Exception to LogError, LogSevere, LogFatal, LogDebugError, LogDebugSevere, and LogDebugFatal.")]
        ///<summary>
        /// Log exception information.
        ///</summary>
        ///<param name="message">Denotes the text to log.</param>
        ///<param name="exception">The exception to log.</param>
        ///<param name="isDebugError">Denotes whether to write as a debug/error or standard error.</param>
        public void LogException(string message, Exception exception, bool isDebugError = false)
        {
            if (isDebugError)
                LogDebugError($"{message} -> {exception.ToString()}");
            else
                LogError($"{message} -> {exception.ToString()}");
        }
        
        private InternalLogger GetInternalLogger() => internalLogger;

        public string Label
        {
            get => loggerTag.ToUpper();
            set => loggerTag = value.ToUpper();
        }

        public static VerbosityLevel InternalVerbosityLevel
        {
            get => InternalLogger.INSTANCE.Verbosity;
            set => InternalLogger.INSTANCE.Verbosity = value;
        }
    }
}