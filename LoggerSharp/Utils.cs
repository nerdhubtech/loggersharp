﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LoggerSharp.Linq
{
    public static class Utils
    {
		public static T[] Skip<T>(this T[] tarr, int count)
		{
			var x = (count - 1);
			if (count <= 0 || x < 0)
				return new T[0];
			else
			{
				var l = tarr.ToList();
				l.RemoveAt(x);
				return l.ToArray();
			}
		}

        public static string[] Split(this string str, string spl)
        {
            List<string> sL = new List<string>();
            string sx = "";
            for (int i = 0; i < str.Length; i++)
            {
                var c = str[i];
                //Do a constant test.
                if (sx.EndsWith(spl))
                {
                    //Manually remove the spl string, add the SX value to SL, then clear sx and continue parsing.
                    sx = sx.Remove(sx.Length - spl.Length, spl.Length);
                    sL.Add(sx);
                    sx = "";
                    //Add to SX.
                    sx += c;
                }
                else
                    sx += c;
            }
            if (!(string.IsNullOrEmpty(sx) && string.IsNullOrWhiteSpace(sx)))
            {
                if (sx.EndsWith(spl))
                    sx = sx.Remove(sx.Length - spl.Length, spl.Length);
                else if (sx.StartsWith(spl))
                    sx = sx.Remove(0, spl.Length);
                sL.Add(sx);
            }
            return sL.ToArray();
        }
    }
}
