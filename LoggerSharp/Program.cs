using System;
using System.Text;
using System.Linq;
using System.IO;
using LoggerSharp;

///<summary>
/// This namespace is for testing and debugging purposes only, do not submit bug reports for this namespace.
///</summary>
namespace LoggerSharp.Tests
{
    class Program
    {
        static void Main(string[] args)
            => new Program().Run(args);

        Logger logger;

        public Program()
        {
            logger = new Logger(this, "DUMMY", true, true, new FileInfo($"{Directory.GetCurrentDirectory()}/Logs/logger.log"));
            //Set the local Verbosity.
            logger.Verbosity = VerbosityLevel.INFO;
        }

        public void Run(string[] args)
        {
            Console.WriteLine("Using manual logger.");
            Log(logger);
            Console.WriteLine("Using INTERNAL logger.");
            var lggr = InternalLogger.INSTANCE;
            Log(lggr);
        }

        private void Log(Logger logger)
        {
            logger.LogInfo("Hello World!");
            logger.LogWarning("Hello World!");
            logger.LogError("Hello World!");
            logger.LogSevere("Hello World!");
            logger.LogFatal("Hello World!");

            logger.LogDebugInfo("Hello World!");
            logger.LogDebugError("Hello World");

            logger.LogError("Hello World", new Exception("This is a Hello World error."));

        }
    }
}
