using System;
using System.IO;
using System.Diagnostics;
using System.Text;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

namespace LoggerSharp
{
    public struct LogStatus
    {
        private string name;
        private int status;
        private ConsoleColor? color;

        internal LogStatus(int status, string name, ConsoleColor? color = null)
        {
            this.status = status;
            this.name = name;
            this.color = color;
        }

        //Gets the number representing the current status.
        public int GetStatus() => status;
        //Gets the color of the current status.
        //TODO: Create "Color Table" for all log status codes that logstatus can pull from and that the user can change.
        public ConsoleColor? GetStatusColor() => color;
        //Gets the name of the status.
        public string GetStatusName() => name;

        /* internal bool IsRange(int min, int max)
        {
            for (int i = 0; i < max; i++)
            {
                if (!(i >= min))
                    i++;
                else
                {
                    if (GetStatus().Equals(i))
                        return true;
                    else
                        continue;
                }
            }
            return false;
        }*/

        ///<summary>
        /// The log status is <c>OFF</c>, meaning no status will be returned.
        ///</summary>
        public static LogStatus OFF => StatusFactory.INSTANCE.GetStatus(nameof(OFF));
        ///<summary>
        /// The log status is <c>DEBUG_FATAL</c>, meaning no status will be returned.
        ///</summary>
        public static LogStatus DEBUG_FATAL => StatusFactory.INSTANCE.GetStatus(nameof(DEBUG_FATAL));
        ///<summary>
        /// The log status is <c>DEBUG_SEVERE</c>, meaning no status will be returned.
        ///</summary>
        public static LogStatus DEBUG_SEVERE => StatusFactory.INSTANCE.GetStatus(nameof(DEBUG_SEVERE));
        ///<summary>
        /// The log status is <c>DEBUG_ERROR</c>, meaning no status will be returned.
        ///</summary>
        public static LogStatus DEBUG_ERROR => StatusFactory.INSTANCE.GetStatus(nameof(DEBUG_ERROR));
        ///<summary>
        /// The log status is <c>DEBUG_WARNING</c>, meaning no status will be returned.
        ///</summary>
        public static LogStatus DEBUG_WARNING => StatusFactory.INSTANCE.GetStatus(nameof(DEBUG_WARNING));
        ///<summary>
        /// The log status is <c>DEBUG_INFO</c>, meaning no status will be returned.
        ///</summary>
        public static LogStatus DEBUG_INFO => StatusFactory.INSTANCE.GetStatus(nameof(DEBUG_INFO));
        ///<summary>
        /// The log status is <c>INFO</c>, meaning no status will be returned. Used any time general information needs to be sent to the user.
        ///</summary>
        public static LogStatus INFO => StatusFactory.INSTANCE.GetStatus(nameof(INFO));
        ///<summary>
        /// The log status is <c>WARNING</c>, meaning no status will be returned. Used any time there's mild issues the user should look out for, but has no impact on general use.
        ///</summary>
        public static LogStatus WARNING => StatusFactory.INSTANCE.GetStatus(nameof(WARNING));
        ///<summary>
        /// The log status is <c>ERROR</c>, meaning no status will be returned. Used when there's an issue that could cause a degraded experience for the user.
        ///</summary>
        public static LogStatus ERROR => StatusFactory.INSTANCE.GetStatus(nameof(ERROR));
        ///<summary>
        /// The log status is <c>SEVERE</c>, meaning no status will be returned. Used when there's an issue that could cause the application to crash.
        ///</summary>
        public static LogStatus SEVERE => StatusFactory.INSTANCE.GetStatus(nameof(SEVERE));
        ///<summary>
        /// The log status is <c>FATAL</c>, meaning no status will be returned. Used when the application was forced to termiante due to an issue or error.
        ///</summary>
        public static LogStatus FATAL => StatusFactory.INSTANCE.GetStatus(nameof(FATAL));

        //TODO: Support custom LogStatus(es).
    }

    internal sealed class StatusFactory
    {
        internal static StatusFactory INSTANCE = new StatusFactory();
        private List<LogStatus> statuses;

        StatusFactory()
        {
            statuses = new List<LogStatus>(11);
            //Register defaults.
            AddRange(new LogStatus[] 
            {
                new LogStatus(-6, nameof(LogStatus.OFF)),
                new LogStatus(-5, nameof(LogStatus.DEBUG_FATAL), ConsoleColor.DarkCyan),
                new LogStatus(-4, nameof(LogStatus.DEBUG_SEVERE), ConsoleColor.DarkCyan),
                new LogStatus(-3, nameof(LogStatus.DEBUG_ERROR), ConsoleColor.DarkCyan),
                new LogStatus(-2, nameof(LogStatus.DEBUG_WARNING), ConsoleColor.DarkCyan),
                new LogStatus(-1, nameof(LogStatus.DEBUG_INFO), ConsoleColor.DarkCyan),
                new LogStatus(0, nameof(LogStatus.INFO)),
                new LogStatus(1, nameof(LogStatus.WARNING), ConsoleColor.DarkYellow),
                new LogStatus(2, nameof(LogStatus.ERROR), ConsoleColor.Red),
                new LogStatus(3, nameof(LogStatus.SEVERE), ConsoleColor.DarkRed),
                new LogStatus(4, nameof(LogStatus.FATAL), ConsoleColor.DarkGray)
            });
        }

        internal void AddRange(LogStatus[] statuses)
        {
            foreach (LogStatus status in statuses)
                Add(status);
        }

        internal void Add(LogStatus status)
            => statuses.Add(status);

        internal void Remove(LogStatus status)
            => statuses.Remove(status);

        internal IReadOnlyList<LogStatus> GetStatuses() => statuses;

        internal LogStatus GetStatus(int id)
        {
            foreach (LogStatus status in GetStatuses())
            {
                if (status.GetStatus().Equals(id))
                    return status;
                else
                    continue;
            }
            return LogStatus.OFF;
        }

        internal LogStatus GetStatus(string name)
        {
            foreach (LogStatus status in GetStatuses())
            {
                if (status.GetStatusName().ToUpper().Equals(name.ToUpper()))
                    return status;
                else
                    continue;
            }
            return LogStatus.OFF;
        }

        internal bool ContainsStatus(LogStatus status)
        {
            foreach (LogStatus status_ in GetStatuses())
            {
                if (status_.Equals(status))
                    return true;
                else
                    continue;
            }
            return false;
        }
    }
}